/*
Problem Statement
Given an N * NN∗N matrix where each row and column is sorted in ascending order, find the Kth smallest element in the matrix.

Example 1:

Input: Matrix=[
    [2, 6, 8], 
    [3, 7, 10],
    [5, 8, 11]
  ], 
  K=5
Output: 7
Explanation: The 5th smallest number in the matrix is 7.
*/

import java.util.*;

class Node {
	int row;
	int col;
	
	public Node(int row, int col) {
		this.row = row;
		this.col = col;
	}
}

class KthSmallestInSortedMatrix {
	 
  public static int findKthSmallest(int[][] matrix, int k) {
    PriorityQueue<Node> minHeap = new PriorityQueue<>( (a,b) -> matrix[a.row][a.col] - matrix[b.row][b.col] );
	int i = 0;
	
	while(i<matrix.length && i<k) {
		minHeap.add(new Node(i, 0));
		i++;
	}
	
	Node result = new Node(0,0);
	i = 0;
	while(!minHeap.isEmpty()){
		result = minHeap.poll();
		if ( ++i == k ) {
			break;
		}
		result.col++;
		if ( result.col < matrix[0].length )
			minHeap.add(result);
		
	}
	
    return matrix[result.row][result.col];
  }

  public static void main(String[] args) {
    int matrix[][] = { { 2, 6, 8 }, { 3, 7, 10 }, { 5, 8, 11 } };
    int result = KthSmallestInSortedMatrix.findKthSmallest(matrix, 5);
    System.out.print("Kth smallest number is: " + result);
  }
}