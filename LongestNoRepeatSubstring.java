/*
Problem Statement
Given a string, find the length of the longest substring which has no repeating characters.

Example 1:

Input: String="aabccbb"
Output: 3
Explanation: The longest substring without any repeating characters is "abc".
Example 2:

Input: String="abbbb"
Output: 2
Explanation: The longest substring without any repeating characters is "ab".
Example 3:

Input: String="abccde"
Output: 3
Explanation: Longest substrings without any repeating characters are "abc" & "cde".
*/

import java.util.*;
public class LongestNoRepeatSubstring {
	public static int solution(String s) {
		int start = 0, end = 0, result = 0;
		HashMap<Character, Integer> hash = new HashMap<>();
		
		while(end < s.length()) {
			Character c = s.charAt(end);
			if ( hash.get(c) == null ) {
				result = Math.max(result, end-start+1);
			}
			else{
				start = Math.max(start, end);
			}
			hash.put(c, end);
			end++;
		}
		
		return result;
	}
	
	public static void main(String args[]) {
		LongestNoRepeatSubstring a = new LongestNoRepeatSubstring();
		System.out.println(a.solution("aaaaaaa"));
	}
}