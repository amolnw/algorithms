/*
Problem Statement
Given an array of unsorted numbers and a target number, find all unique quadruplets in it whose sum is equal to the target number.

Example 1:

Input: [4, 1, 2, -1, 1, -3], target=1
Output: [-3, -1, 1, 4], [-3, 1, 1, 2]
Explanation: Both the quadruplets add up to the target.
Example 2:

Input: [2, 0, -1, 1, -2, 2], target=2
Output: [-2, 0, 2, 2], [-1, 0, 1, 2]
Explanation: Both the quadruplets add up to the target.
*/
import java.util.*;

class QuadrupleSumToTarget {
	
	
  public static void findElements(int arr[], int target, int i, int j, List<List<Integer>> result ) {
	  
	  int start = j + 1;
	  int end = arr.length - 1;
	  
	  while(start < end) {
		  int sum = arr[i] + arr[j] + arr[start] + arr[end];
		  if ( target == sum ) {
			  result.add(Arrays.asList(arr[i], arr[j], arr[start], arr[end]));
			  start++;
			  end--;
			  while ( start < end && arr[start] == arr[start-1] ) {
				  start++;
			  }
			  while ( start < end && arr[end] == arr[end+1] ) {
				  end--;
			  }
		  }
		  else if ( target > sum ) {
			  start++;
		  }
		  else {
			  end--;
		  }
	  }
  }
  
  public static List<List<Integer>> searchQuadruplets(int[] arr, int target) {
    List<List<Integer>> quadruplets = new ArrayList<>();
	Arrays.sort(arr);
	if ( arr.length == 0 )
		return quadruplets;
    for(int i=0;i<arr.length-3;i++){
		if ( i > 0 && arr[i] == arr[i-1] )
			continue;
		
		for(int j=i+1;j<arr.length-2;j++) {
			if ( j > i+1 && arr[j] == arr[j-1] )
				continue;
			findElements(arr, target, i, j, quadruplets);
		}
	}
    return quadruplets;
  }

  public static void main(String[] args) {
    System.out.println(QuadrupleSumToTarget.searchQuadruplets(new int[] { 4, 1, 2, -1, 1, -3 }, 1));
    System.out.println(QuadrupleSumToTarget.searchQuadruplets(new int[] { 2, 0, -1, 1, -2, 2 }, 2));
	System.out.println(QuadrupleSumToTarget.searchQuadruplets(new int[] { 1, 0, -1, 0, -2, 2 }, 0));
  }
}