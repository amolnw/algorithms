/*
Problem Statement
Given ‘M’ sorted arrays, find the smallest range that includes at least one number from each of the ‘M’ lists.

Example 1:

Input: L1=[1, 5, 8], L2=[4, 12], L3=[7, 8, 10]
Output: [4, 7]
Explanation: The range [4, 7] includes 5 from L1, 4 from L2 and 7 from L3.
Example 2:

Input: L1=[1, 9], L2=[4, 12], L3=[7, 10, 16]
Output: [9, 12]
Explanation: The range [9, 12] includes 9 from L1, 12 from L2 and 10 from L3.
*/

import java.util.*;

class Node {
	int listIndex;
	int arrayIndex;
	
	public Node(int listIndex, int arrayIndex){
		this.listIndex = listIndex;
		this.arrayIndex = arrayIndex;
	}
}

class SmallestRange {

  public static int[] findSmallestRange(List<Integer[]> lists) {
    PriorityQueue<Node> minHeap = new PriorityQueue<>((a,b)->lists.get(a.listIndex)[a.arrayIndex] - lists.get(b.listIndex)[b.arrayIndex]);
	int rangeEnd = Integer.MIN_VALUE, smallestRange = Integer.MAX_VALUE;
	for(int i=0;i<lists.size();i++) {
		minHeap.add(new Node(i, 0));
		rangeEnd = Math.max(rangeEnd, lists.get(i)[0]);
	}
	
	int[] result = new int[] { -1, -1 };
	while(minHeap.size() == lists.size()){
		Node temp = minHeap.poll();
		
		if (( rangeEnd - lists.get(temp.listIndex)[temp.arrayIndex]) < smallestRange ) {
			smallestRange = rangeEnd - lists.get(temp.listIndex)[temp.arrayIndex];
			result[0] = lists.get(temp.listIndex)[temp.arrayIndex];
			result[1] = rangeEnd;
		}
		temp.arrayIndex++;
		
		if ( temp.arrayIndex < lists.get(temp.listIndex).length ) {
			minHeap.add(temp);
			rangeEnd = Math.max(rangeEnd, lists.get(temp.listIndex)[temp.arrayIndex]);
		}
	}
	
	
    return result;
  }

  public static void main(String[] args) {
    Integer[] l1 = new Integer[] { 1, 5, 8 };
    Integer[] l2 = new Integer[] { 4, 12 };
    Integer[] l3 = new Integer[] { 7, 8, 10 };
    List<Integer[]> lists = new ArrayList<Integer[]>();
    lists.add(l1);
    lists.add(l2);
    lists.add(l3);
    int[] result = SmallestRange.findSmallestRange(lists);
    System.out.print("Smallest range is: [" + result[0] + ", " + result[1] + "]");
	
	l1 = new Integer[] { 4, 10, 15, 24, 26 };
    l2 = new Integer[] { 0, 9, 12, 20 };
    l3 = new Integer[] { 5, 18, 22, 30 };
    lists = new ArrayList<Integer[]>();
    lists.add(l1);
    lists.add(l2);
    lists.add(l3);
    result = SmallestRange.findSmallestRange(lists);
    System.out.print("Smallest range is: [" + result[0] + ", " + result[1] + "]");
  }
}
